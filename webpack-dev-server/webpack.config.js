
// PROYECTO CON DISTINTAS ENTRADAS que tienes archivos JS distintos: Como /home   --  /contact


// Importamos el módulo path y minicssextract de Node
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
//Exportamos el módulo
module.exports = {
    plugins: [
        // Para exportar el css a un archivo externo
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
        })        
        
    ],
    entry: {
        home: path.resolve(__dirname,'src/js/index.js')      
    },
    mode: 'development',
    output: {
        // Resolve devuelve una ruta absoluta para todos los S.O.
        // Dist es una carpeta con un nombre de convención para guardar los archivos compilados
        path: path.resolve(__dirname, 'dist'),
        // NAME es el nombre de los entry
        filename: 'js/[name].js'
    },
    devServer: {
        hot: true,
        open:true,
        port: 9090,
    },
    // Aquí viven los loaders
    module: {
        rules: [
            {
                // Expresión  regular
                test: /\.css$/,
                // Hay que instalar el móudlo css-loader para cargar el css y style para que el html tenga el estilo
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },    
                    'css-loader',                                   
                ],
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),        
        new HtmlWebpackPlugin({
            title: 'Plugins',
        }),
        // Para exportar el css a un archivo externo
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
        }),
      
        
    ],
};

