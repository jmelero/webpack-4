
// PROYECTO CON DISTINTAS ENTRADAS que tienes archivos JS distintos: Como /home   --  /contact


// Importamos el módulo path de Node
const path = require('path');

//Exportamos el módulo

module.exports = {
    entry: {
        home: path.resolve(__dirname,'src/js/index.js'),
        precios: path.resolve(__dirname,'src/js/index.js'),
        contact: path.resolve(__dirname,'src/js/index.js')
    },
    mode: 'development',
    output: {
        // Resolve devuelve una ruta absoluta para todos los S.O.
        // Dist es una carpeta con un nombre de convención para guardar los archivos compilados
        path: path.resolve(__dirname, 'dist'),
        // NAME es el nombre de los entry
        filename: 'js/[name].js'
    }
};

