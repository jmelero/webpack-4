
// PROYECTO CON DISTINTAS ENTRADAS que tienes archivos JS distintos: Como /home   --  /contact


// Importamos el módulo path de Node
const path = require('path');

//Exportamos el módulo

module.exports = {
    entry: {
        home: path.resolve(__dirname,'src/js/index.js')      
    },
    mode: 'development',
    output: {
        // Resolve devuelve una ruta absoluta para todos los S.O.
        // Dist es una carpeta con un nombre de convención para guardar los archivos compilados
        path: path.resolve(__dirname, 'dist'),
        // NAME es el nombre de los entry
        filename: 'js/[name].js'
    },
    // Aquí viven los loaders
    module: {
        rules: [
            {
                // Expresión  regular
                test: /\.css$/,
                // Hay que instalar el móudlo css-loader para cargar el css y style para que el html tenga el estilo
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    }
};

