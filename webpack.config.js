// Importamos el módulo path de Node
const path = require('path');

//Exportamos el módulo

module.exports = {
    entry: './index.js',    
    output: {
        // Resolve devuelve una ruta absoluta para todos los S.O.
        path: path.resolve(__dirname),
        filename: 'bunddle.js'
    }
};

