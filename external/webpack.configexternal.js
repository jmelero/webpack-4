// Importamos el módulo path de Node
const path = require('path');

//Exportamos el módulo

module.exports = {
    entry: path.resolve(__dirname,'index.js'),
    mode: 'development',
    output: {
        // Resolve devuelve una ruta absoluta para todos los S.O.
        // Dist es una carpeta con un nombre de convención para guardar los archivos compilados
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
};

